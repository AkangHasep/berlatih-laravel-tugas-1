@extends('layout.master')

@section('title')
    Edit Cast
@endsection

@section('content')

<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
  <div class="form-group">
    <label>Nama Cast</label>
    <input type="text" name="nama" value="{{$cast->nama}}" class="form-control">
  </div>
  @error('nama')
    <div class="alert alert-danger">Silahkan isi Nama dahulu!</div>
  @enderror
  <div class="form-group">
    <label>Umur Cast</label>
    <input type="text" name="umur" value="{{$cast->umur}}"class="form-control">
  </div>
  @error('umur')
    <div class="alert alert-danger">Silahkan isi Umur dahulu!</div>
  @enderror
  <div class="form-group">
    <label>Bio Cast</label>
     <textarea name="bio" class="form-control">{{$cast->bio}}</textarea>
  </div>
  @error('bio')
    <div class="alert alert-danger">Silahkan isi Bio dahulu!</div>
  @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection