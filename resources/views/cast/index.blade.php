@extends('layout.master')

@section('title')
    Tambah Cast
@endsection

@section('content')

<a href="/cast/create" class="btn btn-secondary mb-3">Tambah Cast</a>

<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama</th>
      <th scope="col">Umur</th>
      <th scope="col">Bio</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
      @forelse ($cast as $key => $item)
        <tr>
          <td>{{$key + 1}}</td>
          <td>{{$item->nama}}</td>
          <td>{{$item->umur}}</td>
          <td>{{$item->bio}}</td>
          <td>
            <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm mb=2">Detail</a>
            <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm mb=2">Edit</a>
            <form action="/cast/{{$item->id}}" method="POST">  
              @csrf
              @method('delete')
              <input type="submit" class="btn btn-danger btn-sm" value="Delete">
            </form>
          </td>
        </tr>
      @empty
        <h1> Data tidak ada</h1>
      @endforelse

  </tbody>
</table>

@endsection
