@extends('layout.master')

@section('title')
    Halaman Welcome
@endsection

@section('content')
    <h2>Selamat Datang {{$namadepan}} {{$namabelakang}} </h2>
    <h3>Terima kasih telah bergabung di Website Kami. Media Belajar kita bersama!</h3>
@endsection