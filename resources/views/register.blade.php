@extends('layout.master')

@section('title')
    Halaman Form
@endsection

@section('content')
<h2>Buat Account Baru</h2>

<h4>Sign Up Form</h4>
<form action="/welcome" method="POST">
    @csrf
    <Label>Fist Name:</Label><br>
        <input type="text" name="nama_depan"><br><br>
    <Label>Last Name:</Label><br>
        <input type="text" name="nama_belakang"><br><br>
    <Label>Password:</Label><br>
        <input type="password" name="password"><br><br> 
    <Label>Gender</Label><br>
        <input type="radio">Male<br>
        <input type="radio">Female<br><br> 
    <Label>Nationality</Label><br>
        <select name="Negara">
            <option value="Negara_1">Indonesia</option>
            <option value="Negara_2">Amerika</option>
            <option value="Negara_3">Inggris</option>
        </select> <br><br>
     <Label>Language Spoken</Label><br>
        <input type="checkbox" name="Bahasa_1">Bahasa Indonesia<br>
        <input type="checkbox" name="Bahasa_2">English<br>
        <input type="checkbox" name="Bahasa_3">Other<br><br>
    <Label>Bio</Label><br>
        <textarea name="message" rows="10" cols="30"></textarea><br><br>
        <input type="submit" value="Signup">
@endsection