<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar()
    {
        return view('register');
    }

    public function kirim(Request $request)
    {
        // dd ($request->all());
        $namadepan = $request ['nama_depan'];
        $namabelakang = $request ['nama_belakang'];

        return view ('welcome', compact("namadepan" , "namabelakang"));
    }

}
