<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@home');

Route::get('/register', 'AuthController@daftar');

Route::post('/welcome', 'AuthController@kirim');

Route::get('/data-table', 'IndexController@table');

// CRUD Cast
// Create
Route::get('/cast/create', 'CastController@create'); // Route menuju form Create
Route::post('/cast', 'CastController@store'); //Route untuk simpan data ke database

// Read
Route::get('/cast', 'CastController@index'); //Route tampilkan list data para pemain film
Route::get('/cast/{cast_id}', 'CastController@show'); //Route tampilkan detail data pemain film berdasarkan id tertentu

// Update
Route::get('/cast/{cast_id}/edit', 'CastController@edit'); //Route tampilkan form untuk edit pemain film dengan id tertentu
Route::put('/cast/{cast_id}', 'CastController@update'); //Route simpan perubahan data pemain film (update) untuk id tertentu

// Delete
Route::delete('/cast/{cast_id}', 'CastController@destroy');//Route hapus data pemain film dengan id tertentu